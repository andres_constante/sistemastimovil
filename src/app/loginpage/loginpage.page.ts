//import { Component, OnInit } from '@angular/core';
import { Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from '../providers/api/api.service';
@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.page.html',
  styleUrls: ['./loginpage.page.scss'],
})
export class LoginpagePage /*implements OnInit*/ {

  constructor(public toastController: ToastController,private router: Router,private route: ActivatedRoute,public api:ApiService) {}
  public username:string;
  public pass:string;

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Las credenciales de acceso no son correctas!',
      duration: 2000
    });
    toast.present();
  } 
  go() {
    this.api.login(this.username,this.pass).then(succ=>{
      if(succ["id"]!=null)
        this.router.navigateByUrl('/home');
    }).catch(succ=>{
      console.log(succ)
      this.presentToast();
    })
    
  }
/*
  ngOnInit() {
  }*/

}
