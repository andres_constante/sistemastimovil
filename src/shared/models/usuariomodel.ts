export class UsuariosModel{
    constructor(
       public id:number,        
       public name:string,
       public email:string,
       public cedula:string,
       public telefono:string,
       public id_tipo_usuario:string,
       public id_perfil:string,
       public nuevo:string,
       public estado:string,
       public id_direccion:string,
       public titulo:string
       
   ){}

   static fromJson(data:any)
   {   
           
       return new UsuariosModel(data.id,data.name,data.email,data.cedula,data.telefono,data.id_tipo_usuario,data.id_perfil,data.nuevo,data.estado,data.id_direccion,data.titulo);
       
   }
   
   
}